"""get_gridfs_files: Get files from gridfs and save in path in you machine."""

__author__ = 'Flavio Gaspareto'
__copyright__ = 'Copyright 2019, UPX'
__credits__ = ['Flavio Gaspareto', 'Renan Leite']

__version__ = '0.1'
__maintainer__ = 'Flavio Gaspareto'
__email__ = 'flavio.gaspareto@upx.com'
__status__ = 'Production'