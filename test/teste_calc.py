from unittest import TestCase
from app.calc import soma, sub


class TesteCalc(TestCase):
    def test_soma(self):
        self.assertEqual(soma(2, 2), 4)

    def test_sub(self):
        self.assertEqual(sub(4, 2), 2)
